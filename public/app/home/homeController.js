(function(){
	"use strict";

	angular.module('home')
		.controller('homeController', homeController);

	function homeController($http){
        let home = this;
        home.postUrl = "https://jsonplaceholder.typicode.com/posts";
        home.userIdUrl = home.postUrl + "?userId=";
        home.allPosts = [];
        home.reverse = false;
        home.sort = "false";
        home.group = "nogroup";
        home.generated = false;
        
        home.getPosts = function(){
            $http.get(home.postUrl).then((res) => {
                home.allPosts = res.data;
                home.postsPerUser = home.processPerUser(res.data);
                home.generated = true;
            }).catch((err) => {
                alert("Unable to generate posts");
            })
        }
        
        home.sortTitle = function(){
            home.reverse = !home.reverse;
        }
        
        home.processPerUser = function(data){
            return data.reduce(function(array, post){
                if(post.userId in array){
                    array[post.userId].push(post);
                }else{
                    array[post.userId] = [];
                    array[post.userId].push(post);
                }
                return array;
            }, []);
        }
        
    }

})();