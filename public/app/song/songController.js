(function(){
	"use strict";

	angular.module("song")
		.controller("songController", songController);

	function songController($http){
		let song = this;
        song.song1 = "https://static.bandlab.com/soundbanks/previews/new-wave-kit.ogg";
        song.song2 = "https://static.bandlab.com/soundbanks/previews/synth-organ.ogg";
        
        song.playlist = [
            {src: song.song1, type: 'audio/ogg'},
            {src: song.song2, type: 'audio/ogg'}
        ];
	}

})();