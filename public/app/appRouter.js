(function(){
    "use strict";
    
    angular.module('app')
        .config(appConfig);
    
    function appConfig($stateProvider, $urlRouterProvider){
        
        $urlRouterProvider.otherwise('/');
        
         $stateProvider.state({
             name: 'home',
             url: '/',
             templateUrl: 'app/home/home.html',
             controller: 'homeController',
             controllerAs: 'home'
         }).state({
             name: 'songs',
             url: '/songs',
             templateUrl: 'app/song/song.html',
             controller: 'songController',
             controllerAs: 'song'
         })
    }
    
})();