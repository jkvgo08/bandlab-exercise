const gulp = require('gulp');
const sass = require('gulp-sass');
const notify = require('gulp-notify');
const nodemon = require('gulp-nodemon');
const livereload = require('gulp-livereload');

//tasks
gulp.task('sass', function(){
    return gulp.src('./public/assets/css/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./public/assets/css'))
        .pipe(livereload())
        .pipe(notify({
            message: "CSS has been sassed!",
            onLast: true
        }));
});

gulp.task('watch', function(){
    livereload.listen();
    gulp.watch('./public/assets/css/*.scss', ['sass']);
});

gulp.task('server', function(){
    nodemon({
        script: 'app.js',
        env: {'NODE_ENV': 'development'}
    }).on('start', ['watch']);
});

//gulp.task('default', gulp.series('server', defaultTask));
gulp.task('default', ['server']);

function defaultTask(done){
    done();
}