const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");

const app = express();
const port = 8088;
app.use(bodyParser.json());
app.use(express.static('public'));
app.use(cors());

app.listen(port);